const gulp = require("gulp");
const concat = require("gulp-concat");
const del = require("del");
const autoprefixer = require("gulp-autoprefixer");

gulp.task("prepare-css", (cb) => {
  gulp.src("src/css/*.css").pipe(concat("all.css")).pipe(gulp.dest("dist/css"));
  // wildcard file path
  cb();
});

gulp.task("prepare-html", (cb) => {
  gulp.src("src/*.html").pipe(gulp.dest("dist"));
  cb();
});

gulp.task("prepare-all", (cb) => {
  gulp.parallel(["prepare-html", "prepare-css"]);
  cb();
});

gulp.task("prepare-img", (cb) => {
  gulp.src("src/image/*.img").pipe(gulp.dest("dist"));
  cb();
});

exports.build = gulp.parallel("prepare-html", "prepare-css", "prepare-img");
